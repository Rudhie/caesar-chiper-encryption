<?php
	class Chipper{
		public $text;

		function enkrip_shift($text){
			$text = strtolower($text);
			$konversi = str_split($text);

			$enkripsi = "";

			foreach ($konversi as $key => $value) {
				if(ord($value) >= 120){
					$enkripsi .= chr((ord($value) - 26) + 3);
				} else {
					$enkripsi .= chr(ord($value)+3);
				}
				
			}

			return "Kata Asli = ".$text."<br/>Kata Hasil = ".$enkripsi;
		}

		function enkrip_rms_mod($text){
			$text = strtolower($text);
			$konversi = str_split($text);

			$enkripsi = "";

			foreach ($konversi as $key => $value) {
				$enkripsi .= chr(( 3 * (ord($value) - 97) % 26 ) + 97);
			}

			return $enkripsi;
		}

	}
?>